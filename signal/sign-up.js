//Variables
const AWS = require('aws-sdk');
const phoneLib = require('phone')
const sns = new AWS.SNS();
const ddb = new AWS.DynamoDB.DocumentClient();
const dynamoTable = 'signal-users'; //Test
const middy = require('middy')
const { urlEncodeBodyParser, validator, httpErrorHandler } = require('middy/middlewares')

//Functions
const addContact = (contactID, phone, phoneCountry) => {
    return ddb.put({
        TableName: 'signal-users',
        Item: {
            'id': contactID,
            'phone': phone,
            'country': phoneCountry,
            'time': Date.now()
        }
    })
}

const sesSubscribe = (phone, phoneCountry) => {
    return sns.subscribe({
        Protocol: 'sms',
        Endpoint: phone,
        TopicArn: 'arn:aws:sns:us-west-2:081758618601:signal-f1'
    }).promise();
}

//Logic
 exports.handler = async (event, context, callback) => {
    let phone = event['phone'];
    let phoneCountry = phoneLib(phone)[1] || '';

    if (phone.length && phoneCountry.length) {

        try {
            const subscribe = await sesSubscribe(phone, phoneCountry);
            await addContact(subscribe.SubscriptionArn,phone,phoneCountry)
            callback(null, { "message": "Success" });
        } catch(error){
            callback(null, { "message": err });
        }

    } else {
        var response = {
            status: 400,
            message:"Error: The Phone Number is not valid, try adding your country prefix (Example +1)"
        }
        callback(JSON.stringify(response));
    }
}