let AWS = require('aws-sdk');
//Variables
const sns = new AWS.SNS();

//Functions
const publishMessage = (message, subject, sender) => {
    return sns.publish({
        Message: message,
        Subject: subject,
        MessageAttributes: {
            'senderID': {
                DataType: 'String',
                StringValue: sender
            }
        },
        MessageStructure: 'String',
        TopicArn: 'arn:aws:sns:us-west-2:081758618601:signal-f1'
    }).promise()
}

//Logic

exports.handler = function (event, context, callback) {
    let subject = event['subject'];
    let sender = event['sender'];
    let message = event['message'];
    

    publishMessage(message, subject, sender).then(data => {
        callback(null, { "message": "Messages Sent to Topic" });
    }).catch(err => {
        callback(null, { "message": err });
    });
}